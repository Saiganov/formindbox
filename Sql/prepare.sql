-- скрипты для подготовки
CREATE TABLE Products (
    id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    title VARCHAR(255) NOT NULL
)
CREATE TABLE Categories (
    id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    title VARCHAR(255) NOT NULL
)
CREATE TABLE Product_Category (
    product_id INT NOT NULL,
    category_id Int NOT NULL,
   FOREIGN KEY(product_id) REFERENCES Products(id),
   FOREIGN KEY(category_id) REFERENCES Categories(id)
)
-- тестовые данные
INSERT INTO Products (title) VALUES ('product 1'), ('product 2'), ('product 3'), ('product 4'), ('product 5'), ('product 6')
INSERT INTO Categories(title) VALUES ('category 1'), ('category 2'), ('category 3')
INSERT INTO Product_Category (product_id, category_id) VALUES (1, 1), (1, 2), (2, 1), (2, 2)
