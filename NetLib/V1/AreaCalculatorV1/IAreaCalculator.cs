﻿namespace AreaCalculatorV1
{
    public interface IAreaCalculator
    {
        double CalculateAreaForCircle(double radius);

        double CalculateAreaForTriangleByGeron
            (double sideA, double sideB, double sideC);

        double CalculateAreaForTriangleByAngleInRadiance(
            double sideA, double sideB, double angle);

        double CalculateAreaForShape(IShape shape);
    }
}