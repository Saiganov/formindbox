﻿using System;

namespace AreaCalculatorV1
{
    public class AreaCalculator : IAreaCalculator
    {
        public double CalculateAreaForCircle(double radius)
        {
            if (radius <= 0)
                throw new ArgumentNullException(
                    nameof(radius), $"{nameof(radius)} has invalid value");

            var area = Math.PI * radius * radius;
            return area;
        }

        public double CalculateAreaForTriangleByGeron(
            double sideA, double sideB, double sideC)
        {
            if(sideA <= 0)
                throw new ArgumentNullException(
                    nameof(sideA), $"{nameof(sideA)} has invalid value");
            if (sideB <= 0)
                throw new ArgumentNullException(
                    nameof(sideB), $"{nameof(sideB)} has invalid value");
            if (sideC <= 0)
                throw new ArgumentNullException(
                    nameof(sideC), $"{nameof(sideC)} has invalid value");

            var perimeterHalf = (sideA + sideB + sideC) / 2;
            var area = Math.Sqrt(perimeterHalf * (perimeterHalf - sideA)
                                                 * (perimeterHalf - sideB) 
                                                 * (perimeterHalf - sideC));
            return area;
        }

        public double CalculateAreaForTriangleByAngleInRadiance(
            double sideA, double sideB, double angle)
        {
            if (sideA <= 0)
                throw new ArgumentNullException(
                    nameof(sideA), $"{nameof(sideA)} has invalid value");
            if (sideB <= 0)
                throw new ArgumentNullException(
                    nameof(sideB), $"{nameof(sideB)} has invalid value");
            if (angle <= 0)
                throw new ArgumentNullException(
                    nameof(angle), $"{nameof(angle)} has invalid value");
            
            var area = (sideA * sideB) / 2 * Math.Sin(angle);
            return area;
        }

        public double CalculateAreaForShape(IShape shape)
        {
            return shape.CalculateArea();
        }
    }
}