﻿namespace AreaCalculatorV1
{
    public interface IShape
    {
        double CalculateArea();
    }
}