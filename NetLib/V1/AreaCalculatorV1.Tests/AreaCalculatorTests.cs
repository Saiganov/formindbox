using System;
using AreaCalculatorV1.Tests.Helpers;
using NUnit.Framework;
using Shouldly;

namespace AreaCalculatorV1.Tests
{
    public class AreaCalculatorTests
    {
        private IAreaCalculator _calculator;

        [SetUp]
        public void Settings()
        {
            _calculator = new AreaCalculator();
        }

        [TestCase(1, 2, 0)]
        [TestCase(1, 0, 2)]
        [TestCase(0, 2, 3)]
        [TestCase(0, -2, -3)]
        [TestCase(1, -2, 0)]
        public void CalculateAreaForTriangleByGeron_PassInvalidSides_ThrowsArgumentException(
            double sideA, double sideB, double sideC)
        {
            Should.Throw<ArgumentNullException>( () =>
                _calculator.CalculateAreaForTriangleByGeron(sideA, sideB, sideC));
        }

        [TestCase(3,3,3, 3.897)]
        [TestCase(4,5,7, 9.797)]
        public void CalculateAreaForTriangleByGeron_PassValidSides_CalculateArea(
            double sideA, double sideB, double sideC, double expectedValue)
        {
            var area = _calculator.CalculateAreaForTriangleByGeron(
                sideA, sideB, sideC);

            area.RoundWithPrecision().ShouldBe(expectedValue.RoundWithPrecision());
        }

        [TestCase(3, 3, 1.0472, 3.897)]
        public void CalculateAreForTriangleByAngle_PassValidParams_CalculateArea(
            double sideA, double sideB, double angle, double expectedValue)
        {
            var area = _calculator.CalculateAreaForTriangleByAngleInRadiance(
                sideA, sideB, angle);

            area.RoundWithPrecision().ShouldBe(expectedValue.RoundWithPrecision());
        }

        [TestCase(0)]
        [TestCase(-1)]
        public void CalculateAreaForCircle_PassInvalidRadius_ThrowArgumentNullException(
            double radius)
        {
            Should.Throw<ArgumentNullException>(() =>
                _calculator.CalculateAreaForCircle(radius));
        }

        [TestCase(10, 314.1593)]
        [TestCase(15, 706.8583)]
        public void CalculateAreaForCircle_PassValidRadius_CalculateArea(
            double radius, double expectedArea)
        {
            var area = _calculator.CalculateAreaForCircle(radius);

            area.RoundWithPrecision().ShouldBe(expectedArea.RoundWithPrecision());
        }
    }
}