﻿using NUnit.Framework;
using Shouldly;

namespace AreaCalculatorV1.Tests.ExtendLibExample
{
    public class SquareCalculatorExtendLibTests
    {
        private IAreaCalculator _calculator;

        [SetUp]
        public void Settings()
        {
            _calculator = new AreaCalculator();
        }

        [Test]
        public void CalculatVeSquareForShape()
        {
            IShape shape = new Square(5);

            _calculator.CalculateAreaForShape(shape).ShouldBe(25);
        }
    }
}