using System;
using NSubstitute;
using NUnit.Framework;
using Shouldly;

namespace AreaCalculatorV2.Tests
{
    public class AreaCalculatorTests
    {
        private IAreaCalculator _calculator;

        [SetUp]
        public void Settings()
        {
            _calculator = new AreaCalculator();
        }
        
        [Test]
        public void CalculateArea_ShapeAreaThrowException_ThrowException()
        {
            var shape = Substitute.For<IShape>();
            shape.Area.Returns(_ => throw new ArgumentNullException());

            Should.Throw<ArgumentNullException>(() => 
                _calculator.CalculateArea(shape));
        }

        [Test]
        public void CalculateArea_ShapeCalculateCorrectArea_ReturnArea()
        {
            var shape = Substitute.For<IShape>();
            shape.Area.Returns(42);

            _calculator.CalculateArea(shape).ShouldBe(shape.Area);
        }
    }
}