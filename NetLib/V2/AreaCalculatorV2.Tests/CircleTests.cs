﻿using System;
using AreaCalculatorV2.Shapes;
using AreaCalculatorV2.Tests.Helpers;
using NUnit.Framework;
using Shouldly;

namespace AreaCalculatorV2.Tests
{
    public class CircleTests
    {
        [TestCase(0)]
        [TestCase(0.0d)]
        [TestCase(-1)]
        public void CreateCircle_PassInvalidRadius_ThrowArgumentNullException(
            double radius)
        {
            Should.Throw<ArgumentNullException>(() =>
                new Circle(radius));
        }

        [TestCase(10, 314.1593)]
        [TestCase(15, 706.8583)]
        public void Area_PassValidRadius_ReturnArea(
            double radius, double expectedArea)
        {
            var circle = new Circle(radius);
            
            circle.Area.RoundWithPrecision().ShouldBe(expectedArea.RoundWithPrecision());
        }
    }
}