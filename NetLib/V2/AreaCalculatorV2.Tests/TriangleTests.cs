﻿using System;
using AreaCalculatorV2.Shapes;
using AreaCalculatorV2.Tests.Helpers;
using NUnit.Framework;
using Shouldly;

namespace AreaCalculatorV2.Tests
{
    public class TriangleTests
    {
        [TestCase(0, 1, 2)]
        [TestCase(1, -1, 3)]
        [TestCase(0, -1, -3)]
        public void CreateTriangle_PassInvalidSides_ThrowArgumentNullException(
            double sideA, double sideB, double sideC)
        {
            Should.Throw<ArgumentNullException>(() =>
                new Triangle(sideA, sideB, sideC));
        }

        [TestCase(3, 3, 3, 3.897)]
        [TestCase(4, 5, 7, 9.797)]
        public void Area_PassValidSides_ReturnArea(
            double sideA, double sideB, double sideC, double expectedArea)
        {
            var triangle = new Triangle(sideA, sideB, sideC);

            triangle.Area.RoundWithPrecision().ShouldBe(expectedArea.RoundWithPrecision());
        }
    }
}