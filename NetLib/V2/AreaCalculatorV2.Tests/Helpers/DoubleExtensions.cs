using System;

namespace AreaCalculatorV2.Tests.Helpers
{
    public static class DoubleExtensions
    {
        public static double RoundWithPrecision(this double num, int precision = 2)
        {
            return Math.Round(num, precision);
        }
    }
}