﻿namespace AreaCalculatorV2
{
    public interface IShape
    {
        double Area { get; }
    }
}