﻿namespace AreaCalculatorV2
{
    public interface IAreaCalculator
    {
        double CalculateArea(IShape shape);
    }
}
