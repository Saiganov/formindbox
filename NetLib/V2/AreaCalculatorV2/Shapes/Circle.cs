﻿using System;

namespace AreaCalculatorV2.Shapes
{
    public class Circle : IShape
    {
        private readonly double _radius;

        public double Area => Math.PI * _radius * _radius;

        public Circle(double radius)
        {
            if (radius <= 0)
            {
                throw new ArgumentNullException(
                    nameof(radius), $"{nameof(radius)} has invalid value!");
            }
            _radius = radius;
        }
    }
}
