﻿using System;

namespace AreaCalculatorV2.Shapes
{
    public class Triangle : IShape
    {
        private readonly double _sideA;
        private readonly double _sideB;
        private readonly double _sideC;

        public double Area
        {
            get
            {
                var perimeterHalf = (_sideA + _sideB + _sideC) / 2;
                var area = Math.Sqrt(perimeterHalf * (perimeterHalf - _sideA)
                                                   * (perimeterHalf - _sideB)
                                                   * (perimeterHalf - _sideC));
                return area;
            }
        }

        public Triangle(double sideA, double sideB, double sideC)
        {
            if (sideA <= 0)
                throw new ArgumentNullException(
                    nameof(sideA), $"{nameof(sideA)} has invalid value");
            if (sideB <= 0)
                throw new ArgumentNullException(
                    nameof(sideB), $"{nameof(sideB)} has invalid value");
            if (sideC <= 0)
                throw new ArgumentNullException(
                    nameof(sideC), $"{nameof(sideC)} has invalid value");


            _sideA = sideA;
            _sideB = sideB;
            _sideC = sideC;
        }
    }
}