﻿namespace AreaCalculatorV2
{
    public class AreaCalculator : IAreaCalculator
    {
        public double CalculateArea(IShape shape)
        {
            return shape.Area;
        }
    }
}